from flask import Flask


app = Flask(__name__)


@app.route('/')
def helloo():
    return 'Hello o o o o o o o'


@app.route('/hello')
def hello():
    return 'Hello, World!'


if __name__ == '__main__':
    # app.run(debug=True)
    app.run(debug=True, host='0.0.0.0', port=5000)
