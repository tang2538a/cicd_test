import requests

class Webhook():
    onechat_url = "https://chat-api.one.th"
    onechatbot_id = "B0e42aac13b8d547ba303b00f8b225aa2"
    onechat_dev_token = "Bearer Bearer A4665996d217651cd9a100f35203b3f6d7f4581c412fa4430a77a6f4851fa74e341b7e45b691a485a88c8f17cf3674e44"

    sendmessage_url = "https://chat-api.one.th/message/api/v1/push_message"
    sendmessage_header = {"Authorization": onechat_dev_token}

    def send_quick_reply(self, one_id, msg, payload):
        TAG = "send_quick_reply:"
        req_body = {
            "to":one_id,
            "bot_id": self.onechatbot_id,
            "message": "ให้ช่วยอะไรดี",
            "quick_reply": payload
        }
        print(TAG, "payload=", payload)
        r = requests.post(self.sendmessage_url, json=payload, headers=self.sendmessage_headers, verify=False)
        return r